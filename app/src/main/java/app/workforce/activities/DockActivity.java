package app.workforce.activities;

import android.content.Context;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentManager.BackStackEntry;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import app.workforce.BaseApplication;
import app.workforce.R;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.global.AppConstants;
import app.workforce.helpers.BasePreferenceHelper;
import app.workforce.interfaces.ILoadingListener;
import app.workforce.interfaces.IPermissionResult;
import app.workforce.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

//import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;


/**
 * This class is marked abstract so that it can pair with Dockable Fragments
 * only. All Classes extending this will inherit this functionality of
 * interaction with menus.
 */
public abstract class DockActivity extends AppCompatActivity implements
        ILoadingListener, View.OnClickListener {

    public static final String KEY_FRAG_FIRST = "firstFrag";

    @BindView(R.id.header_main)
    public TitleBar titleBar;

    @BindView(R.id.progressBr)
    protected ProgressBar progressBar;

  @BindView(R.id.mainFrameLayout)
    protected FrameLayout mainFrameLayout;

    @BindView(R.id.coordinatorLayout)
    protected CoordinatorLayout coordinatorLayout;

    protected BasePreferenceHelper prefHelper;

    BaseFragment baseFragment;
    private BaseFragment previousFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dock);

        prefHelper = new BasePreferenceHelper(this);
        ButterKnife.bind(this);


    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onResume() {
        super.onResume();
    }

    public void addFragmentAndDeleteBackStack(BaseFragment frag, String tag) {

        try {
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();

            transaction.add(getMainFrameLayoutID(), frag, tag);
            transaction
                    .addToBackStack(tag).commitAllowingStateLoss();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDockableFragment(BaseFragment frag, String tag) {

        try {
            FragmentTransaction transaction = getSupportFragmentManager()
                    .beginTransaction();

            transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);


            previousFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.PREVIOUS_FRAGMENT);
            if (previousFragment != null) {
                transaction.hide(previousFragment);
            }

            if (frag.isAdded()) {
                transaction.show(frag);
            } else {
                transaction.add(getMainFrameLayoutID(), frag, tag);
                transaction
                        .addToBackStack(tag).commitAllowingStateLoss();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Fragment getCurrentFragment(){
        return getSupportFragmentManager().findFragmentById(getMainFrameLayoutID());
    }

    public BaseFragment getFragmentByTag(String tag) {
        return (BaseFragment) getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void addAndShowDialogFragment(
            DialogFragment dialog) {
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        dialog.show(transaction, "tag");

    }

    public void prepareAndShowDialog(DialogFragment frag, String TAG,
                                     BaseFragment fragment) {
        FragmentTransaction transaction = fragment.getChildFragmentManager()
                .beginTransaction();
        Fragment prev = fragment.getChildFragmentManager().findFragmentByTag(
                TAG);

        if (prev != null)
            transaction.remove(prev);

        transaction.addToBackStack(null);
        frag.show(transaction, TAG);
    }

    @Override
    public void onBackPressed() {


    }

    public BaseFragment getLastAddedFragment() {
        return baseFragment;
    }


    /**
     * @param entryIndex is the index of fragment to be popped to, for example the
     *                   first fragment will have a index 0;
     */
    public void popBackStackTillEntry(int entryIndex) {
        try {
            if (getSupportFragmentManager() == null)
                return;
            if (getSupportFragmentManager().getBackStackEntryCount() <= entryIndex)
                return;
            BackStackEntry entry = getSupportFragmentManager().getBackStackEntryAt(
                    entryIndex);
            if (entry != null) {
                getSupportFragmentManager().popBackStack(entry.getId(),
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void popFragment() {
        try {
            if (getSupportFragmentManager() == null) {
                return;
            }
            getSupportFragmentManager().popBackStack();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public BaseApplication getMainApplication() {
        return (BaseApplication) getApplication();
    }

    public abstract void onKeyDown();

    public abstract FrameLayout getMainFrameLayout();

    public abstract int getMainFrameLayoutID();

    public abstract void onMenuItemActionCalled(int actionId, String data);

    public abstract void setSubHeading(String subHeadText);

    public abstract boolean isLoggedIn();

    public abstract void hideHeaderButtons(boolean leftBtn, boolean rightBtn);


    public abstract boolean addMultiplePermission(Context context, IPermissionResult permissionResult, int viewID, ArrayList<String> permissions, int REQUEST_PERMISSION_CODE);

    public abstract boolean addMultiplePermission(Context context, BaseFragment fragment, ArrayList<String> permissions, int REQUEST_PERMISSION_CODE);

    public abstract boolean addSinglePermission(Context context, BaseFragment fragment, String permissionRequest, int REQUEST_PERMISSION_CODE);

    public abstract CoordinatorLayout getCoordinatorLayout();



}
