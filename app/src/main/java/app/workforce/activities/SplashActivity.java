package app.workforce.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import app.workforce.R;
import app.workforce.global.AppConstants;
import app.workforce.global.ExceptionHandler;
import app.workforce.helpers.BasePreferenceHelper;
import app.workforce.helpers.UIHelper;
import app.workforce.retrofit.WebService;
import app.workforce.retrofit.WebServiceFactory;
import app.workforce.ui.views.AnyTextView;

import java.security.MessageDigest;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends Activity {

    boolean workComplete = false;

    final int TIME_INTERVAL_TO_CHECK = 500;// in millis
    final int MIN_TIME_INTERVAL_FOR_SPLASH = 2500; // in millis

    Timer checkWorkTimer;


    protected BasePreferenceHelper prefHelper;

    private static WebService digitalxpressWebService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

         prefHelper = new BasePreferenceHelper(this);


        launchTimerAndTask();


        //logTokens();
    }

    private void launchTimerAndTask() {

        // Launch timer to test image changing and background threads work
        checkWorkTimer = new Timer();
        checkWorkTimer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                if (workComplete) {
                    initNextActivity();
                }
            }

        }, MIN_TIME_INTERVAL_FOR_SPLASH, TIME_INTERVAL_TO_CHECK);

        new Thread(backgroundWork).start();
    }

    Runnable backgroundWork = new Runnable() {

        @Override
        public void run() {

            workComplete = true;
        }

    };

    private void initNextActivity() {
        checkWorkTimer.cancel();
        showMainActivity();

    }

    private void showMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void logTokens() {

        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getApplicationContext().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA-1");
                md.update(signature.toByteArray());
                Log.d("SHA1-KeyHash:::",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));

                md = MessageDigest.getInstance("MD5");
                md.update(signature.toByteArray());
                Log.d("MD5-KeyHash:::",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));

                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("TAG", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (Exception e) {

        }
    }
}