package app.workforce.activities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import app.workforce.R;
import app.workforce.fragments.HomeFragment;
import app.workforce.fragments.SideMenuFragment;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.helpers.UIHelper;
import app.workforce.interfaces.IPermissionResult;
import app.workforce.observer.ObservableRegistrar;

//import com.kbeanie.imagechooser.api.ChooserType;
//import com.kbeanie.imagechooser.api.ChosenFile;
//import com.kbeanie.imagechooser.api.ChosenImage;
//import com.kbeanie.imagechooser.api.ChosenImages;
//import com.kbeanie.imagechooser.api.ChosenVideo;
//import com.kbeanie.imagechooser.api.ChosenVideos;
//import com.kbeanie.imagechooser.api.FileChooserListener;
//import com.kbeanie.imagechooser.api.FileChooserManager;
//import com.kbeanie.imagechooser.api.ImageChooserListener;
//import com.kbeanie.imagechooser.api.ImageChooserManager;
//import com.kbeanie.imagechooser.api.VideoChooserListener;
//import com.kbeanie.imagechooser.api.VideoChooserManager;

public class MainActivity extends DockActivity implements OnClickListener, Observer {

    private ImageLoader imageLoader;

    private MainActivity mContext;

    private boolean isLoading;

    private float lastTranslate = 0.0f;

    //For image and video chooser
//    private FileChooserManager fileChooserManager;
//    private VideoChooserManager videoChooserManager;
//    private ImageChooserManager imageChooserManager;
    private String filePath;

    private int chooserType;
    private final static String TAG = "ICA";

    private boolean isActivityResultOver = false;

    private String originalFilePath;
    private String thumbnailFilePath;
    private String thumbnailSmallFilePath;

    private MultimediaPickerDelegate multimediaPickerDelegate;

    private ObservableRegistrar observableRegistrar = null;

    private BaseFragment baseFragment;

    //private AnyTextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = ImageLoader.getInstance();

        //logTokens();
        mContext = this;


        titleBar.setMenuButtonListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //mMenuDialogFragment.show(getSupportFragmentManager(), ContextMenuDialogFragment.TAG);
                addDockableFragment(SideMenuFragment.newInstance(), "SideMenuFragment");
            }
        });

        titleBar.setBackButtonListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (isLoading) {
                    UIHelper.showLongToastInCenter(getApplicationContext(),
                            R.string.message_wait);
                } else {

                    onBackPressed();
                    UIHelper.hideSoftKeyboard(getApplicationContext(),
                            titleBar);
                }
            }
        });

        initiateWebService();

        initDesiredFragment();

        observableRegistrar = ObservableRegistrar.getInstance();
        observableRegistrar.addObserver(this);

    }


//    public FileChooserManager getFileChooserManager() {
//        return fileChooserManager;
//    }
//
//    public ImageChooserManager getImageChooserManager() {
//        return imageChooserManager;
//    }
//
//    public VideoChooserManager getVideoChooserManager() {
//        return videoChooserManager;
//    }

    private void initiateWebService() {

    }


    public void initDesiredFragment() {
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());

        prefHelper.setLoginStatus(true);

        if (prefHelper.isLogin()) {
            addDockableFragment(HomeFragment.newInstance(), "HomeFragment");
        }
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {

                    baseFragment = (BaseFragment) manager.findFragmentById(getMainFrameLayoutID());
                    if (baseFragment != null) {
                        baseFragment.OnResume();

                    }
                }
            }
        };

        return result;
    }

    @Override
    public void onLoadingStarted() {

        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.bringToFront();

        }
        isLoading = true;
    }

    @Override
    public void onLoadingFinished() {

        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
        }
        isLoading = false;

    }

    @Override
    public void onProgressUpdated(int percentLoaded) {

    }

    @Override
    public FrameLayout getMainFrameLayout() {
        return mainFrameLayout;
    }

    @Override
    public int getMainFrameLayoutID() {
        return mainFrameLayout.getId();
    }

    @Override
    public void onMenuItemActionCalled(int actionId, String data) {

    }

    @Override
    public void setSubHeading(String subHeadText) {

    }

    @Override
    public boolean isLoggedIn() {
        return false;
    }

    @Override
    public void hideHeaderButtons(boolean leftBtn, boolean rightBtn) {
    }

    @Override
    public void onBackPressed() {
        onKeyDown();


    }

    @Override
    public void onKeyDown() {
        try {
            baseFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(getMainFrameLayoutID());
            baseFragment.onKeyDown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Drawable resize(Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * 0.5), (int) (bitmap.getHeight() * 0.5), false);
        return new BitmapDrawable(getResources(), bitmapResized);
    }


    private void notImplemented() {
        UIHelper.showSnackBar(this, "Coming Soon");
    }


    @Override
    public void onClick(View v) {

    }


//    @Override
//    public void onImageChosen(final ChosenImage image) {
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                Log.i(TAG, "Chosen BlogImage: O - " + image.getFilePathOriginal());
//                Log.i(TAG, "Chosen BlogImage: T - " + image.getFileThumbnail());
//                Log.i(TAG, "Chosen BlogImage: Ts - " + image.getFileThumbnailSmall());
//                isActivityResultOver = true;
//                originalFilePath = image.getFilePathOriginal();
//                thumbnailFilePath = image.getFileThumbnail();
//                thumbnailSmallFilePath = image.getFileThumbnailSmall();
//                //pbar.setVisibility(View.GONE);
//                if (image != null) {
//                    Log.i(TAG, "Chosen BlogImage: Is not null");
//
//                    multimediaPickerDelegate.setImage(thumbnailFilePath);
//                    //loadImage(imageViewThumbnail, image.getFileThumbnail());
//                } else {
//                    Log.i(TAG, "Chosen BlogImage: Is null");
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onImagesChosen(final ChosenImages images) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Log.i(TAG, "On Images Chosen: " + images.size());
//                onImageChosen(images.getImage(0));
//            }
//        });
//    }
//
//    @Override
//    public void onFileChosen(final ChosenFile file) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                // progressBar.setVisibility(View.INVISIBLE);
//                multimediaPickerDelegate.setFilePath(file.getFilePath());
//                populateFileDetails(file);
//            }
//        });
//    }
//
//    private void populateFileDetails(ChosenFile file) {
//        StringBuffer text = new StringBuffer();
//        text.append("File name: " + file.getFileName() + "\n\n");
//        text.append("File path: " + file.getFilePath() + "\n\n");
//        text.append("Mime type: " + file.getMimeType() + "\n\n");
//        text.append("File extn: " + file.getExtension() + "\n\n");
//        text.append("File size: " + file.getFileSize() / 1024 + "KB");
//
//        //Toast.makeText(this,text.toString(),Toast.LENGTH_LONG).show();
//    }
//
//
//    @Override
//    public void onVideoChosen(final ChosenVideo video) {
//
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                if (video != null) {
//                    Log.i(TAG, "Chosen BlogImage: Is not null");
//
//                    multimediaPickerDelegate.setVideo(video.getVideoFilePath(), video.getThumbnailPath());
//                    //loadImage(imageViewThumbnail, image.getFileThumbnail());
//                } else {
//                    Log.i(TAG, "Chosen BlogImage: Is null");
//                }
//            }
//        });
//    }
//
//    @Override
//    public void onVideosChosen(ChosenVideos videos) {
//
//    }
//
//    @Override
//    public void onError(final String reason) {
//        runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                Log.i(TAG, "OnError: " + reason);
//                // pbar.setVisibility(View.GONE);
//                UIHelper.showSnackBar(MainActivity.this, reason);
//
//            }
//        });
//    }


    public interface MultimediaPickerDelegate {

        void setImage(String imagePath);

        void setFilePath(String filePath);

        void setVideo(String videoPath, String thumbNailPart);

    }

    public void setMultimediaPickerDelegate(MultimediaPickerDelegate multimediaPickerDelegate) {
        this.multimediaPickerDelegate = multimediaPickerDelegate;
    }

//    public void reinitializeImageChooser() {
//        imageChooserManager = new ImageChooserManager(this, chooserType, false);
//        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        imageChooserManager.setExtras(bundle);
//        imageChooserManager.setImageChooserListener(this);
//        imageChooserManager.reinitialize(filePath);
//    }
//
//    public void reinitializeVideoChooser() {
//        videoChooserManager = new VideoChooserManager(this, chooserType, false);
//        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, false);
//        videoChooserManager.setExtras(bundle);
//        videoChooserManager.setVideoChooserListener(this);
//        videoChooserManager.reinitialize(filePath);
//    }
//
//    public void reinitializeFileChooser() {
//        fileChooserManager = new FileChooserManager(this);
//        fileChooserManager.setFileChooserListener(this);
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i(TAG, "Saving Stuff");
        Log.i(TAG, "File Path: " + filePath);
        Log.i(TAG, "Chooser Type: " + chooserType);
        outState.putBoolean("activity_result_over", isActivityResultOver);
        outState.putInt("chooser_type", chooserType);
        outState.putString("media_path", filePath);
        outState.putString("orig", originalFilePath);
        outState.putString("thumb", thumbnailFilePath);
        outState.putString("thumbs", thumbnailSmallFilePath);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("chooser_type")) {
                chooserType = savedInstanceState.getInt("chooser_type");
            }
            if (savedInstanceState.containsKey("media_path")) {
                filePath = savedInstanceState.getString("media_path");
            }
            if (savedInstanceState.containsKey("activity_result_over")) {
                isActivityResultOver = savedInstanceState.getBoolean("activity_result_over");
                originalFilePath = savedInstanceState.getString("orig");
                thumbnailFilePath = savedInstanceState.getString("thumb");
                thumbnailSmallFilePath = savedInstanceState.getString("thumbs");
            }
        }
        Log.i(TAG, "Restoring Stuff");
        Log.i(TAG, "File Path: " + filePath);
        Log.i(TAG, "Chooser Type: " + chooserType);
        Log.i(TAG, "Activity Result Over: " + isActivityResultOver);
        if (isActivityResultOver) {
            //populateData();
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

//    public void chooseVideo(BaseFragment fragment) {
//        chooserType = ChooserType.REQUEST_PICK_VIDEO;
//        videoChooserManager = new VideoChooserManager(fragment, chooserType, true);
//        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, false);
//        videoChooserManager.setExtras(bundle);
//        videoChooserManager.setVideoChooserListener(this);
//        videoChooserManager.clearOldFiles();
//        try {
//            //pbar.setVisibility(View.VISIBLE);
//            filePath = videoChooserManager.choose();
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void chooseImage(BaseFragment fragment) {
//        chooserType = ChooserType.REQUEST_PICK_PICTURE;
//        imageChooserManager = new ImageChooserManager(fragment, chooserType, false);
//        Bundle bundle = new Bundle();
//        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        imageChooserManager.setExtras(bundle);
//        imageChooserManager.setImageChooserListener(this);
//        imageChooserManager.clearOldFiles();
//        try {
//            //pbar.setVisibility(View.VISIBLE);
//            filePath = imageChooserManager.choose();
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void takePicture(BaseFragment fragment) {
//        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
//        imageChooserManager = new ImageChooserManager(fragment, chooserType, false);
//        imageChooserManager.setImageChooserListener(this);
//        try {
//            //pbar.setVisibility(View.VISIBLE);
//            filePath = imageChooserManager.choose();
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void pickFile() {
//        fileChooserManager = new FileChooserManager(this);
//        fileChooserManager.setFileChooserListener(this);
//        try {
//            // progressBar.setVisibility(View.VISIBLE);
//            fileChooserManager.choose();
//        } catch (Exception e) {
//            // progressBar.setVisibility(View.INVISIBLE);
//            e.printStackTrace();
//        }
//    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public boolean addMultiplePermission(Context context, IPermissionResult permissionResult, int viewID, ArrayList<String> permissions, int REQUEST_PERMISSION_CODE) {

        List<String> listPermissionsNeeded = new ArrayList<>();


        for (int i = 0; i < permissions.size(); i++) {
            if (ContextCompat.checkSelfPermission(context, permissions.get(i)) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permissions.get(i));
            }
        }

        if (listPermissionsNeeded.isEmpty()) {
            return true;
        } else {

            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION_CODE);

            return false;
        }
    }

    @Override
    public boolean addMultiplePermission(Context context, BaseFragment fragment, ArrayList<String> permissions, int REQUEST_PERMISSION_CODE) {

        List<String> listPermissionsNeeded = new ArrayList<>();


        for (int i = 0; i < permissions.size(); i++) {
            if (ContextCompat.checkSelfPermission(context, permissions.get(i)) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permissions.get(i));
            }
        }

        if (listPermissionsNeeded.isEmpty()) {
            return true;
        } else {
            if (fragment == null) {
                ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray
                        (new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION_CODE);
            } else {
                fragment.requestPermissions(listPermissionsNeeded.toArray
                        (new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION_CODE);
            }
            return false;
        }

    }

    @Override
    public boolean addSinglePermission(Context context, BaseFragment fragment, String permissionRequest, int REQUEST_PERMISSION_CODE) {
        int permission = ContextCompat.checkSelfPermission(context, permissionRequest);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            fragment.requestPermissions(new String[]{permissionRequest}, REQUEST_PERMISSION_CODE);
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {


        }
    }


    @Override
    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }


    //Call happens in FireBaseMessagingReceiver
    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        observableRegistrar.deleteObserver(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        return super.dispatchTouchEvent(event);
    }


}
