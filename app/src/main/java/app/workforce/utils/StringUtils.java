package app.workforce.utils;


import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class StringUtils {

    public static boolean isNull(String str) {
        return str == null ;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static boolean isEmpty(ArrayList<?> collection) {
        return collection == null || collection.size() == 0;
    }

    public static boolean isEmpty(List<?> collection) {
        return collection == null || collection.size() == 0;
    }

    public static boolean isEmptyOrZero(String str) {
        return str == null || str.equals("0");
    }


}
