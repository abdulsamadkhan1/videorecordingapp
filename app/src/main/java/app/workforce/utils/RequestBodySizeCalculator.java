package app.workforce.utils;


import java.io.File;

public class RequestBodySizeCalculator {


    private static long totalRequestBodySizeInBytes = 0;
    private static int totalRequestBodySizeMB = 0;

    public static void resetCounter(){
        totalRequestBodySizeInBytes = 0;
        totalRequestBodySizeMB = 0;
    }

    public static int getRequestBodySizeInMB() {

        return totalRequestBodySizeMB = (int) (totalRequestBodySizeInBytes / (1024 * 1024));
    }

    public static void calculateFileSize(File file){

        if(file != null) {
            totalRequestBodySizeInBytes = totalRequestBodySizeInBytes + file.length();
        }

    }

}
