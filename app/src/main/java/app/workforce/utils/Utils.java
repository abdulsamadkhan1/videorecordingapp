package app.workforce.utils;


import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;

import app.workforce.BaseApplication;
import app.workforce.autolinktextview.AutoLinkMode;
import app.workforce.autolinktextview.RegexParser;
import app.workforce.global.AppConstants;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    private static boolean isValidRegex(String regex) {
        return regex != null && !regex.isEmpty() && regex.length() > 2;
    }

    public static boolean validateURL(String url) {
        Pattern pattern = Pattern.compile(AppConstants.URL_REGEX);
        Matcher matcher;
        matcher = pattern.matcher(url);
        return matcher.matches();
    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(RegexParser.EMAIL_PATTERN);
        Matcher matcher;
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean validatePhone(String phone) {
        Pattern pattern = Pattern.compile(RegexParser.PHONE_PATTERN);
        Matcher matcher;
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public static boolean validateCustomRegex(String text) {
        Pattern pattern = Pattern.compile(RegexParser.CUSTOM_REGEX_PATTERN);
        Matcher matcher;
        matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static String getRegexByAutoLinkMode(AutoLinkMode anAutoLinkMode, String customRegex) {
        switch (anAutoLinkMode) {
            case MODE_HASHTAG:
                return RegexParser.HASHTAG_PATTERN;
            case MODE_MENTION:
                return RegexParser.MENTION_PATTERN;
            case MODE_URL:
                return RegexParser.URL_PATTERN;
            case MODE_PHONE:
                return RegexParser.PHONE_PATTERN;
            case MODE_EMAIL:
                return RegexParser.EMAIL_PATTERN;
            case MODE_CUSTOM:
                if (!Utils.isValidRegex(customRegex)) {
                    Log.e("AutoLinkTextView", "Your custom regex is null, returning URL_PATTERN");
                    return RegexParser.URL_PATTERN;
                } else {
                    return customRegex;
                }
            default:
                return RegexParser.URL_PATTERN;
        }
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String getAppVersionCode(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;

            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static long getMultiMediaFileDuration(Context context, String fileName) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(Utils.generateFileDirectory(fileName).getAbsolutePath());
            return Long.valueOf(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }

    public static long getMultiMediaFileDuration(String filePath) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(filePath);
            return Long.valueOf(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }

    public static File generateFile(String filePath) {

        return new File(filePath);
    }

    public static File generateFileDirectory() {

        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        filePath += "/" + AppConstants.EXTERNAL_DIRECTORY_NAME;

        return new File(filePath);
    }

    public static File generateFileDirectory(String fileName) {

        String filePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        filePath += "/" + AppConstants.EXTERNAL_DIRECTORY_NAME + "/" + fileName;

        File file = new File(filePath);

        mountingRoot(file);

        return file;
    }

    public static void makeFileDirectory() {

        File folder = new File(Environment.getExternalStorageDirectory() + "/" + AppConstants.EXTERNAL_DIRECTORY_NAME);
        if (!folder.exists()) {
            //folder /SoundRecorder doesn't exist, create the folder
            folder.mkdir();
        }

    }

    public static String getAudioPath(Context context, Uri uri) {
        String[] data = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics.heightPixels;

    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        return displayMetrics.widthPixels;
    }

    public static void mountingRoot(final File file) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            try {
                MediaScannerConnection.scanFile(BaseApplication.getAppContext(), new String[]{

                                file.getAbsolutePath()},

                        null, new MediaScannerConnection.OnScanCompletedListener() {

                            public void onScanCompleted(String path, Uri uri)

                            {
                                Log.d("Scan", "Completed");

                            }

                        });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            try {
                final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()));
                BaseApplication.getAppContext().sendBroadcast(intent);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static Uri getURIFromStringPath(String path){
        return Uri.fromFile(new File(path));
    }

    public static void playNotificationSound(Context context){

        try {
            MediaPlayer player = MediaPlayer.create(context, Settings.System.DEFAULT_NOTIFICATION_URI);
            player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
