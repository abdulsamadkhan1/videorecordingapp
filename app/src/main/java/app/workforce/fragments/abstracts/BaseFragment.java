package app.workforce.fragments.abstracts;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.andreabaccega.formedittextvalidator.Validator;

import app.workforce.R;
import app.workforce.activities.DockActivity;
import app.workforce.activities.MainActivity;
import app.workforce.global.AppConstants;
import app.workforce.global.GetColor;
import app.workforce.helpers.BasePreferenceHelper;
import app.workforce.helpers.GPSTrackingHelper;
import app.workforce.helpers.UIHelper;
import app.workforce.observer.ObservableRegistrar;
import app.workforce.ui.dialogs.ConfirmationDialog;
import app.workforce.ui.dialogs.DialogFactory;
import app.workforce.ui.views.AnyEditTextView;
import app.workforce.ui.views.TitleBar;

import java.util.Observable;
import java.util.Observer;


public abstract class BaseFragment extends Fragment implements Observer {

    protected Handler handler = new Handler();

    protected BasePreferenceHelper prefHelper;


    protected GPSTrackingHelper mGpsTracker;

    private DockActivity dockActivity;

    private ObservableRegistrar observableRegistrar = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGpsTracker = new GPSTrackingHelper(getDockActivity());
        prefHelper = new BasePreferenceHelper(getDockActivity());

        observableRegistrar = ObservableRegistrar.getInstance();
        observableRegistrar.addObserver(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    public void OnResume() {
        AppConstants.PREVIOUS_FRAGMENT = this.getTag();

        setTitleBar(getTitleBar());

        if (this.getView() != null) {
            UIHelper.hideSoftKeyboard(getDockActivity(), this.getView());
        }

        getDockActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dockActivity = (DockActivity) context;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (getDockActivity().getWindow() != null)
            if (getDockActivity().getWindow().getDecorView() != null)
                UIHelper.hideSoftKeyboard(getDockActivity(), getDockActivity()
                        .getWindow().getDecorView());

    }


    public void loadingStarted() {
        try {
          /*  if (getParentFragment() != null)
                ((ILoadingListener) getParentFragment()).onLoadingStarted();
            else*/
            getDockActivity().onLoadingStarted();

            isLoading = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadingFinished() {
        try {
           /* if (getParentFragment() != null)
                ((ILoadingListener) getParentFragment()).onLoadingFinished();
            else if (getDockActivity() != null)*/
            getDockActivity().onLoadingFinished();

            isLoading = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        // else
        // ( (ILoadingListener) super.getParentFragment() ).onLoadingFinished();
    }

    protected DockActivity getDockActivity() {
        return dockActivity;

    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    protected TitleBar getTitleBar() {
        return getMainActivity().titleBar;
    }

    public String getTitleName() {
        return "";
    }

    /**
     * This is called in the end to modify titlebar. after all changes.
     *
     * @param titleBar
     */
    public void setTitleBar(TitleBar titleBar) {
        getDockActivity().titleBar.setBackgroundColor(GetColor.getColor(getDockActivity(), R.color.theme_color));
        titleBar.showTitleBar();
        // titleBar.refreshListener();
    }

    /**
     * Gets the preferred height for each item in the ListView, in pixels, after
     * accounting for screen density. ImageLoader uses this value to resize
     * thumbnail images to match the ListView item height.
     *
     * @return The preferred height in pixels, based on the current theme.
     */
    protected int getListPreferredItemHeight() {
        final TypedValue typedValue = new TypedValue();

        // Resolve list item preferred height theme attribute into typedValue
        getActivity().getTheme().resolveAttribute(
                android.R.attr.listPreferredItemHeight, typedValue, true);

        // Create a new DisplayMetrics object
        final DisplayMetrics metrics = new android.util.DisplayMetrics();

        // Populate the DisplayMetrics
        getActivity().getWindowManager().getDefaultDisplay()
                .getMetrics(metrics);

        // Return theme value based on DisplayMetrics
        return (int) typedValue.getDimension(metrics);
    }

    protected String getStringTrimed(AnyEditTextView edtView) {
        return edtView.getText().toString().trim();
    }

    /**
     * This generic method to add validator to a text view should be used
     * FormEditText
     * <p/>
     * Usage : Takes Array of AnyEditTextView ;
     *
     * @return void
     */
    protected void addEmptyStringValidator(AnyEditTextView... allFields) {

        for (AnyEditTextView field : allFields) {
            field.addValidator(new EmptyStringValidator());
        }

    }

    protected void notImplemented() {
        UIHelper.showSnackBar(getActivity(), "Coming Soon");
    }

    protected void serverNotFound() {
        UIHelper.showSnackBar(getActivity(),
                "Unable to connect to the server, "
                        + "are you connected to the internet?");
    }

    /**
     * This generic null string validator to be used FormEditText
     * <p/>
     * Usage : formEditText.addValicator(new EmptyStringValidator);
     *
     * @return Boolean and setError on respective field.
     */
    protected class EmptyStringValidator extends Validator {

        public EmptyStringValidator() {
            super("The field must not be empty");
        }

        @Override
        public boolean isValid(EditText et) {
            return et.getText().toString().trim().length() >= 1;
        }

    }

    /**
     * Trigger when receives broadcasts from device to check wifi connectivity
     * using connectivity manager
     * <p/>
     * Usage : registerBroadcastReceiver() on resume of activity to receive
     * notifications where needed and unregisterBroadcastReceiver() when not
     * needed.
     *
     * @return The connectivity of wifi/mobile carrier connectivity.
     */

    protected BroadcastReceiver mConnectionReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isWifiConnected = false;
            boolean isMobileConnected = false;

            ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (networkInfo != null)
                isWifiConnected = networkInfo.isConnected();

            networkInfo = connMgr
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (networkInfo != null)
                isMobileConnected = networkInfo.isConnected();

            Log.d("NETWORK STATUS", "wifi==" + isWifiConnected + " & mobile=="
                    + isMobileConnected);
        }
    };

    private boolean isLoading;

    protected void finishLoading() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                loadingFinished();
            }
        });
    }

    protected boolean checkLoading() {
        if (isLoading) {
            UIHelper.showSnackBar(getActivity(),
                    R.string.message_wait);
            return false;
        } else {
            return true;
        }

    }

    public void onKeyDown() {
        loadingFinished();

        if (getDockActivity().getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getDockActivity().popFragment();
        } else {

            final ConfirmationDialog dialog = DialogFactory.getConfirmationDialog(getDockActivity());
            dialog.show();

            dialog.setQuestionText(getString(R.string.message_quit_heading));
            dialog.setDialogText(getString(R.string.message_quit));
            dialog.setPositiveButton("YES", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDockActivity().finish();
                }
            });
            dialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getDockActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = getDockActivity().getCurrentFocus();
        if (view == null) {
            view = new View(getDockActivity());
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        observableRegistrar.deleteObserver(this);
    }

    public void afterKeyDown() {

    }

    /*public boolean validateScamForm(){
        return false;
    }*/


}
