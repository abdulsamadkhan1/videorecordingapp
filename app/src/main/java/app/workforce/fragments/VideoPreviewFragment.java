package app.workforce.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.VideoResult;

import java.lang.ref.WeakReference;

import app.workforce.R;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.ui.views.TitleBar;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPreviewFragment extends BaseFragment {

    @BindView(R.id.video)
    VideoView videoView;

    private static WeakReference<VideoResult> videoResult;

    public static void setVideoResult(@Nullable VideoResult result) {
        videoResult = result != null ? new WeakReference<>(result) : null;
    }


    public static VideoPreviewFragment newInstance() {

        return new VideoPreviewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.video_preview_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playVideo();
            }
        });

        final VideoResult result = videoResult == null ? null : videoResult.get();
        if (result == null) {
            getDockActivity().finish();
            return;
        }

        MediaController controller = new MediaController(getDockActivity());
        controller.setAnchorView(videoView);
        controller.setMediaPlayer(videoView);
        videoView.setMediaController(controller);
        videoView.setVideoURI(Uri.fromFile(result.getFile()));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                ViewGroup.LayoutParams lp = videoView.getLayoutParams();
                float videoWidth = mp.getVideoWidth();
                float videoHeight = mp.getVideoHeight();
                float viewWidth = videoView.getWidth();
                lp.height = (int) (viewWidth * (videoHeight / videoWidth));
                videoView.setLayoutParams(lp);
                playVideo();

                if (result.isSnapshot()) {
                    // Log the real size for debugging reason.
                    Log.e("VideoPreview", "The video full size is " + videoWidth + "x" + videoHeight);
                }
            }
        });


    }

    void playVideo() {
        if (videoView.isPlaying()) return;
        videoView.start();
    }



    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.setSubHeading("Video Preview Fragment");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!getDockActivity().isChangingConfigurations()) {
            setVideoResult(null);
        }
    }
}
