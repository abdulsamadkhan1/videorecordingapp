package app.workforce.global;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import app.workforce.R;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.helpers.UIHelper;
import app.workforce.interfaces.IOnLoadMoreListener;
import app.workforce.ui.adapters.RecyclerViewPagingAdapter;

public class EndlessScroll<T> extends RecyclerView.OnScrollListener {

    private Context context;
    private BaseFragment baseFragment;

    private IOnLoadMoreListener iOnLoadMoreListener;
    //private LinearLayoutManager layoutManager;

    private boolean isLoading;

    private int childCount;
    private int firstVisibleItem;
    private int lasttVisibleItem;
    private int totalVisibleItem;

    private int totalItem;
    private int currentPage = 0;
    private int lastPage;


    private RecyclerViewPagingAdapter<T> adapter;

    public EndlessScroll(Context context, BaseFragment baseFragment, IOnLoadMoreListener iOnLoadMoreListener, int totalItem, int lastPage, RecyclerViewPagingAdapter<T> adapter) {

        this.context = context;
        this.baseFragment = baseFragment;

        this.iOnLoadMoreListener = iOnLoadMoreListener;

        this.totalItem = totalItem;
        this.lastPage = lastPage;

        this.adapter = adapter;

    }

    public EndlessScroll(Context context, BaseFragment baseFragment, IOnLoadMoreListener iOnLoadMoreListener, int totalItem, int lastPage) {

        this.context = context;
        this.baseFragment = baseFragment;

        this.iOnLoadMoreListener = iOnLoadMoreListener;

        this.totalItem = totalItem;
        this.lastPage = lastPage;


    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void resetCurrentPage() {
        currentPage = 1;
    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        synchronized (this) {

            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                firstVisibleItem = (((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition());
                lasttVisibleItem = (((LinearLayoutManager) layoutManager).findLastVisibleItemPosition());
                totalVisibleItem = (((LinearLayoutManager) layoutManager).getItemCount());
            }

            if (lasttVisibleItem != -1) {
                if (lasttVisibleItem == totalVisibleItem - 1) {
                    if (iOnLoadMoreListener != null) {

                        if (InternetConnectionChecker.checkConnection(context)) {
                            currentPage += 1;
                            if (currentPage <= lastPage) {

                                if (adapter != null) {
                                    adapter.displayLoadingRow(true);
                                }
                                iOnLoadMoreListener.onLoadMore(currentPage);

                            }
                            isLoading = true;
                        } else {
                            UIHelper.showSnackBar(context, baseFragment.getView(), context.getResources().getString(R.string.msg_connection_error));

                            if (adapter != null) {
                                adapter.displayLoadingRow(false);
                            }
                            adapter.notifyDataSetChanged();
                        }
                    }

                }
            }
        }

    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }
}
