package app.workforce.global;

public enum RecyclerItemType {

    VIEW_TYPE_ITEM(12),
    VIEW_TYPE_LOADING(13);

    private final int itemType;

    RecyclerItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getValue() {
        return itemType;
    }
}
