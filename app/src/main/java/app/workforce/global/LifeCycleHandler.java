package app.workforce.global;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import app.workforce.activities.DockActivity;
import app.workforce.activities.MainActivity;
import app.workforce.helpers.BasePreferenceHelper;

public class LifeCycleHandler implements Application.ActivityLifecycleCallbacks {

    protected BasePreferenceHelper prefHelper;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        prefHelper = new BasePreferenceHelper(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        if (activity instanceof MainActivity) {
            prefHelper.setIsAppBG(false);


        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (activity instanceof MainActivity) {
            prefHelper.setIsAppBG(false);



        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        prefHelper.setIsAppBG(true);
    }

    @Override
    public void onActivityStopped(Activity activity) {
       /* if (activity instanceof MainActivity) {
            prefHelper.setIsAppBG(true);
        }*/
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        /*if (activity instanceof MainActivity) {
            prefHelper.setIsAppBG(true);
        }*/
    }
}
