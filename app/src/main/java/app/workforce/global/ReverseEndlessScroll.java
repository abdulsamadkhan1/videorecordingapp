package app.workforce.global;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import app.workforce.R;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.helpers.UIHelper;
import app.workforce.interfaces.IOnLoadMoreListener;
import app.workforce.ui.adapters.RecyclerViewPagingAdapter;

public class ReverseEndlessScroll<T> extends RecyclerView.OnScrollListener {

    private Context context;
    private BaseFragment baseFragment;

    private IOnLoadMoreListener iOnLoadMoreListener;
    private LinearLayoutManager layoutManager;

    private boolean isLoading;

    private int childCount;
    private int firstVisibleItem;
    private int totalVisibleItem;

    private int previousTotal;
    private int totalItem;
    private int currentPage;
    private int lastPage;

    private RecyclerViewPagingAdapter<T> adapter;

    public ReverseEndlessScroll(Context context, BaseFragment baseFragment, IOnLoadMoreListener iOnLoadMoreListener, LinearLayoutManager layoutManager, int totalItem, int currentPage, int lastPage, RecyclerViewPagingAdapter<T> adapter) {

        this.context = context;
        this.baseFragment = baseFragment;

        this.iOnLoadMoreListener = iOnLoadMoreListener;
        this.layoutManager = layoutManager;

        this.totalItem = totalItem;
        this.currentPage = currentPage;
        this.lastPage = lastPage;

        this.adapter = adapter;

    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        synchronized (this) {
            childCount = layoutManager.getChildCount();
            totalVisibleItem = layoutManager.getItemCount();
            firstVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition();


            if (firstVisibleItem == adapter.getItemCount() - 1) {
                if (iOnLoadMoreListener != null) {
                    if (InternetConnectionChecker.checkConnection(context)) {
                        currentPage += 1;
                        if (currentPage <= lastPage) {

                            adapter.displayLoadingRow(true);
                            iOnLoadMoreListener.onLoadMore(currentPage);
                        }


                        isLoading = true;
                    } else {
                        UIHelper.showSnackBar(context, baseFragment.getView(), context.getResources().getString(R.string.msg_connection_error));

                        adapter.displayLoadingRow(false);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        }

        /*totalVisibleItem = layoutManager.getItemCount();
        firstVisibleItem = layoutManager.findLastVisibleItemPosition();

        if (!isLoading && totalVisibleItem <= (firstVisibleItem + totalItem)) {
            if (iOnLoadMoreListener != null) {

                currentPage += 1;
                if(currentPage <= lastPage) {
                    iOnLoadMoreListener.onLoadMore(currentPage);
                }
            }
            isLoading = true;
        }*/
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }
}
