package app.workforce.global;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class UTCConvertor {

    public static long convertToUTC(String dateTime){
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(dateTime);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormatter.setTimeZone(TimeZone.getDefault());
            String result = dateFormatter.format(value);
            Date resultDate = dateFormatter.parse(result);
            long timeInMili = resultDate.getTime();

            return timeInMili;
        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }
}
