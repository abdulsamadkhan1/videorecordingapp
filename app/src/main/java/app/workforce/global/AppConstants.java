package app.workforce.global;

import app.workforce.BaseApplication;
import app.workforce.R;

public class AppConstants {

    //For File Savings
    public static final String EXTERNAL_DIRECTORY_NAME = "TEMPLATE_APP_NAME";
    public static final String PHOTO_NAME_PREFIX = "TEMPLATE_APP_NAME";


    //for push notification
    public static final String NotificationTitle = "title";
    public static final String NotificationBody = "body";


    public static String PREVIOUS_FRAGMENT = "HomeFragment";

    //Regex for Web url validator
    public static final String URL_REGEX = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";

}
