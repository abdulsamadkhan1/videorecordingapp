package app.workforce.global;


import java.util.ArrayList;
import java.util.Arrays;

public class CSVToArrayListConverter {

    public static ArrayList<String> convert(String CSV) {

        CSV = CSV.replace("\"", "");
        CSV = CSV.replace("[", "");
        CSV = CSV.replace("]", "");

        ArrayList<String> scamIdsCollection = new ArrayList<String>(Arrays.asList(CSV.split(",")));

        return scamIdsCollection;

    }
}
