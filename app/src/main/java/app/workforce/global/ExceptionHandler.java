package app.workforce.global;

import android.content.Context;
import android.view.View;

import app.workforce.R;
import app.workforce.activities.SplashActivity;
import app.workforce.helpers.UIHelper;

import java.io.IOException;

public class ExceptionHandler {


    private static Context mContext;
    private static Throwable mThrowable;


    public static void handleException(Context context, Throwable throwable) {
        mContext = context;
        mThrowable = throwable;

        if (throwable instanceof IOException) {
            handleNoInternetConnectionError();
        } else {
            if (!(context instanceof SplashActivity)) {
                UIHelper.showSnackBar(mContext, context.getString(R.string.went_wrong));

            } else {
                UIHelper.showLongToastInCenter(context, context.getString(R.string.went_wrong));

            }
        }


    }

    public static void handleException(Context context, View view, Throwable throwable) {
        mContext = context;
        mThrowable = throwable;

        if (throwable instanceof IOException) {
            handleNoInternetConnectionError(view);
        } else {

            UIHelper.showSnackBar(mContext, view, mThrowable.getMessage());

        }


    }

    private static void handleNoInternetConnectionError(View view) {
        UIHelper.showSnackBar(mContext, view, mContext.getResources().getString(R.string.msg_connection_error));
    }


    private static void handleNoInternetConnectionError() {
        if (!(mContext instanceof SplashActivity)) {
            UIHelper.showSnackBar(mContext, mContext.getResources().getString(R.string.msg_connection_error));

        } else {
            UIHelper.showLongToastInCenter(mContext, mContext.getResources().getString(R.string.msg_connection_error));

        }
    }
}
