package app.workforce.global;


public enum DeviceType {

    ANDROID("0"),
    IOS("1");

    private final String id;

    DeviceType(String id) {
        this.id = id;
    }

    public String getValue() {
        return id;
    }
}
