package app.workforce;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.StrictMode;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import app.workforce.global.LifeCycleHandler;


import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.L;

//import com.squareup.otto.Bus;


public class BaseApplication extends MultiDexApplication {

    //private static Bus bus;
    private static Context context = null;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

        disableStrictMode();

    }


    private void disableStrictMode() {

        if(Build.VERSION.SDK_INT >= 24) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            builder.detectFileUriExposure();
        }

    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        registerActivityLifecycleCallbacks(new LifeCycleHandler());

        initImageLoader();

        BaseApplication.context = getApplicationContext();

    }

    public void initImageLoader() {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.color.black)
                .showImageOnFail(R.color.black).resetViewBeforeLoading(true)
                .cacheInMemory(true).cacheOnDisc(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .displayer(new FadeInBitmapDisplayer(850))
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
        L.disableLogging();
    }

    public static Context getAppContext() {
        return BaseApplication.context;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }



   /* public static Bus getBus() {
        return bus;
    }*/


}
