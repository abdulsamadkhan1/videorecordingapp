package app.workforce.retrofit;


import android.app.NotificationManager;
import android.content.Context;
import androidx.core.app.NotificationCompat;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class OKHttpClientCreator {

    private static NotificationManager mNotifyManager;
    private  static NotificationCompat.Builder mBuilder;

    public static OkHttpClient createCustomInterceptorClient(Context context) {

        /*mNotifyManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle("Upload ScamVideo")
                .setContentText("Uploading in progress")
                .setSmallIcon(R.drawable.ic_launcher);*/


        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new CustomInterceptor(progressListener))
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();


        return client;


    }

    public static OkHttpClient createDefaultInterceptorClient(Context context) {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        return originalResponse.newBuilder()
                                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                                .build();
                    }
                })
                .build();


        return client;

    }

    final static ProgressResponseBody.ProgressListener progressListener = new ProgressResponseBody.ProgressListener() {
        @Override
        public void update(long bytesRead, long contentLength, boolean done) {

            int percent = (int) ((100 * bytesRead) / contentLength);

            /*if(AppConstants.IS_SHOW_NOTIFICATION){

                mBuilder.setProgress(100, percent, false);
                mNotifyManager.notify(1, mBuilder.build());

                if(percent == 100){
                    mBuilder.setContentText("Upload complete")
                            // Removes the progress bar
                            .setProgress(0,0,false);
                    mNotifyManager.notify(1, mBuilder.build());

                    AppConstants.IS_SHOW_NOTIFICATION = false;
                }

            }*/

        }
    };

}
