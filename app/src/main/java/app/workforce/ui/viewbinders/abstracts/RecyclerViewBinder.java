package app.workforce.ui.viewbinders.abstracts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.workforce.global.BaseItemViewHolder;
import app.workforce.ui.adapters.RecyclerViewAdapter;

import java.util.List;

public abstract class RecyclerViewBinder<T> extends RecyclerViewAdapter {

    private int itemResId;
    private View view;

    public RecyclerViewBinder(Context context, OnViewHolderClick<T> listener, int itemResId) {
        super(context, listener);

        this.itemResId = itemResId;

    }

    public RecyclerViewBinder(Context context, OnViewHolderClick<T> listener) {
        super(context, listener);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(itemResId, viewGroup, false);
        view.setTag(createItemViewHolder(view, viewType));

        return view;
    }

    @Override
    protected void bindView(Object item, ViewHolder viewHolder, int position) {
    }


    public abstract BaseItemViewHolder createItemViewHolder(View view, int viewType);




}
