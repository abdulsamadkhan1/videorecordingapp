package app.workforce.ui.viewbinders.abstracts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.workforce.global.BaseItemViewHolder;
import app.workforce.global.RecyclerItemType;
import app.workforce.ui.adapters.RecyclerViewPagingAdapter;

import java.util.List;

public abstract class RecyclerPagingViewBinder<T> extends RecyclerViewPagingAdapter {

    private int itemResId;
    private int loadingItemResId;
    private View view;

    public RecyclerPagingViewBinder(Context context, OnViewHolderClick<T> listener, int itemResId, int loadingItemResId) {
        super(context, listener);

        this.itemResId = itemResId;
        this.loadingItemResId = loadingItemResId;
    }

    public RecyclerPagingViewBinder(Context context, OnViewHolderClick<T> listener, int loadingItemResId) {
        super(context, listener);

        this.loadingItemResId = loadingItemResId;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (RecyclerItemType.VIEW_TYPE_ITEM.getValue() == viewType) {
            view = inflater.inflate(itemResId, viewGroup, false);
            view.setTag(createItemViewHolder(view, viewType));
        } else if (RecyclerItemType.VIEW_TYPE_LOADING.getValue() == viewType) {
            view = inflater.inflate(loadingItemResId, viewGroup, false);
            view.setTag(createLoadingViewHolder(view));
        }

        return view;
    }

    @Override
    protected void bindView(Object item, ViewHolder viewHolder, int position) {}

    public abstract BaseItemViewHolder createItemViewHolder(View view, int viewType);

    public abstract BaseLoadingViewHolder createLoadingViewHolder(View view);


    public static class BaseLoadingViewHolder {}
}
