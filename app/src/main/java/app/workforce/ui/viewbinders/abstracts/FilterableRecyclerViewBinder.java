package app.workforce.ui.viewbinders.abstracts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import app.workforce.global.BaseItemViewHolder;
import app.workforce.ui.adapters.FilterableRecyclerViewAdapter;
import app.workforce.ui.adapters.RecyclerViewAdapter;
import com.google.common.base.Function;

import java.util.List;

public abstract class FilterableRecyclerViewBinder<T> extends FilterableRecyclerViewAdapter {

    private int itemResId;
    private View view;

    public FilterableRecyclerViewBinder(Context context, List<T> arrayList, FilterableRecyclerViewAdapter.OnViewHolderClick<T> listener, int itemResId, Function<T, String> converter) {
        super(context, arrayList, listener, converter);

        this.itemResId = itemResId;

    }

    public FilterableRecyclerViewBinder(Context context, List<T> arrayList, FilterableRecyclerViewAdapter.OnViewHolderClick<T> listener, Function<T, String> converter) {
        super(context, arrayList, listener, converter);



    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(itemResId, viewGroup, false);
        view.setTag(createItemViewHolder(view, viewType));

        return view;
    }

    @Override
    protected void bindView(Object item, ViewHolder viewHolder, int position) {
    }

    protected abstract BaseItemViewHolder createItemViewHolder(View view, int viewType);




}