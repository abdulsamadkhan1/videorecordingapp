package app.workforce.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import app.workforce.R;
import app.workforce.ui.views.AnyTextView;

public class ConfirmationDialog extends Dialog{

    private Activity mActivity;
    private AnyTextView mTextQuestion;
    private AnyTextView mTextDialog;
    private AnyTextView mPositive, mNegative;

    private View view;

    public ConfirmationDialog(Activity activity) {
        super(activity);
        // TODO Auto-generated constructor stub
        this.mActivity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        LayoutInflater inflater = LayoutInflater.from(mActivity);
        view = inflater.inflate(R.layout.layout_custom_dialog, null, false);

        setContentView(view);

        mTextQuestion = (AnyTextView)findViewById(R.id.textQuestion);
        mTextDialog = (AnyTextView)findViewById(R.id.textDialog);
        mPositive = (AnyTextView) findViewById(R.id.btnPositive);
        mNegative = (AnyTextView) findViewById(R.id.btnNegative);
    }

    public void setQuestionText(String text){
        mTextQuestion.setText(text);
    }

    public void setQuestionText(int textId){
        mTextQuestion.setText(textId);
    }

    public void setDialogText(String text){
        mTextDialog.setText(text);
    }

    public void setDialogText(int textId){
        mTextDialog.setText(textId);
    }

    public void setPositiveButton(String text, View.OnClickListener listener){
        mPositive.setText(text);
        mPositive.setOnClickListener(listener);
    }
    public void setPositiveButton(int textId, View.OnClickListener listener){
        mPositive.setText(textId);
        mPositive.setOnClickListener(listener);
    }

    public void setNegativeButton(String text, View.OnClickListener listener){
        mNegative.setText(text);
        mNegative.setOnClickListener(listener);
    }
    public void setNegativeButton(int textId, View.OnClickListener listener){
        mNegative.setText(textId);
        mNegative.setOnClickListener(listener);
    }

    public View getView(){
        return this.view;
    }


}
