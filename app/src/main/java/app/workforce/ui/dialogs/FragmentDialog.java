package app.workforce.ui.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import app.workforce.R;
import app.workforce.fragments.abstracts.BaseFragment;
import app.workforce.observer.ObservableRegistrar;

import java.util.Observable;
import java.util.Observer;

public class FragmentDialog extends DialogFragment implements Observer {

    private BaseFragment fragment;

    private ObservableRegistrar observableRegistrar;

    private int height = 0;
    private int width = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        observableRegistrar = ObservableRegistrar.getInstance();
        observableRegistrar.addObserver(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_fragment_dialog, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setView();

    }

    public void setDialogDimens(int height, int width) {
        this.height = height;
        this.width = width;
    }

    private void adjustDialog(int height, int width) {
        getDialog().getWindow().setLayout(width, height);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.app_white_rounded_rectangle);


    }

    private void adjustDialog() {
        int width = getResources().getDimensionPixelSize(R.dimen.x320);
        //int height = getResources().getDimensionPixelSize(R.dimen.x320);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.app_white_rounded_rectangle);
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    public void setFragment(BaseFragment fragment) {
        this.fragment = fragment;
    }

    private void setView() {

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment, "AdvancedSearchFragment");
        transaction.commit();
    }

    //call happens in DialogFragments like AdvancedSearchFragment, CustomSupportMapFragment, RecordVoiceFragment and e.t.c
    @Override
    public void update(Observable observable, Object o) {



    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
        if (getDialog() == null) {
            return;
        }
        if (height == 0 && width == 0) {
            adjustDialog();
        } else {
            adjustDialog(height, width);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        observableRegistrar = ObservableRegistrar.getInstance();
        observableRegistrar.deleteObserver(this);
    }


}