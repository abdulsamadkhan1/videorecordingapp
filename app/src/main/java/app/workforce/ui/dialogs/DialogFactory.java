package app.workforce.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

public class DialogFactory {

    public static Dialog createProgressDialog(Activity activity, String title,
                                              String loadingMessage) {
        ProgressDialog prDialog = new ProgressDialog(activity);
        prDialog.setTitle(title);
        prDialog.setMessage(loadingMessage);
        return prDialog;
    }

    public static Dialog createQuitDialog(Activity activity,
                                          DialogInterface.OnClickListener dialogPositive, int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(messageId)
                .setCancelable(true)
                .setPositiveButton(android.R.string.yes, dialogPositive)
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();

                            }
                        });
        return builder.create();

    }

    public static ConfirmationDialog getConfirmationDialog(Activity activity) {

        final ConfirmationDialog dialog = new ConfirmationDialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return dialog;
    }

    public static WebViewDialog getWebViewDialog(Activity activity) {

        final WebViewDialog dialog = new WebViewDialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return dialog;
    }

    public static Dialog createSimpleDialog(Activity activity,
                                            DialogInterface.OnClickListener dialogPositive, int messageId,
                                            int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(messageId)
                .setCancelable(true)
                .setPositiveButton(android.R.string.yes, dialogPositive)
                .setNegativeButton(android.R.string.no,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();

                            }
                        });
        return builder.create();

    }

    public static Dialog createMessageDialog(Activity activity,
                                             DialogInterface.OnClickListener dialogPositive,
                                             CharSequence message, String titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(titleId)
                .setMessage(message).setCancelable(true)
                .setPositiveButton(android.R.string.ok, dialogPositive);
        return builder.create();

    }

    public static Dialog createMessageDialog2(Activity activity,
                                              CharSequence message, String positiveText, String negativeText,
                                              DialogInterface.OnClickListener dialogPositive) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(android.R.string.dialog_alert_title)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(positiveText, dialogPositive)
                .setNegativeButton(negativeText,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();

                            }
                        });
        return builder.create();

    }


}
