package app.workforce.ui.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import app.workforce.global.RecyclerItemType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class RecyclerViewPagingAdapter<T> extends RecyclerView.Adapter<RecyclerViewPagingAdapter.ViewHolder> {

    private List<T> items;
    private Context context;
    private OnViewHolderClick<T> listener;

    private boolean displayLoadingRow = true;

    public RecyclerViewPagingAdapter(Context context, OnViewHolderClick<T> listener) {

        this.context = context;
        this.listener = listener;
        items = new ArrayList<>();

    }

    public interface OnViewHolderClick<T> {
        void onRecyclerItemLongClick(View view, int position, T item);
        void onRecyclerItemClick(View view, int position, T item);
    }

    protected abstract View createView(Context context, ViewGroup viewGroup, int viewType);

    protected abstract void bindView(T item, RecyclerViewPagingAdapter.ViewHolder viewHolder, int position);

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(createView(context, viewGroup, viewType), listener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewPagingAdapter.ViewHolder holder, int position) {
        bindView(getItem(position), holder, position);
    }


    @Override
    public int getItemCount() {
        return displayLoadingRow ? items.size() + 1 : items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isLoadingRow(position) ? RecyclerItemType.VIEW_TYPE_LOADING.getValue() : RecyclerItemType.VIEW_TYPE_ITEM.getValue();
    }

    @Override
    public long getItemId(int position) {
        return isLoadingRow(position) ? RecyclerView.NO_ID : position;
    }


    boolean isDisplayLoadingRow() {
        return displayLoadingRow;
    }

    public void displayLoadingRow(boolean displayLoadingRow) {
        this.displayLoadingRow = displayLoadingRow;
    }

    boolean isLoadingRow(int position) {
        return displayLoadingRow && position == getLoadingRowPosition();
    }

    private int getLoadingRowPosition() {
        return displayLoadingRow ? getItemCount() - 1 : -1;
    }

    public T getItem(int index) {
        return ((items != null && index < items.size()) ? items.get(index) : null);
    }

    public Context getContext() {
        return context;
    }

    public void setList(List<T> list) {
        items = list;
    }

    public List<T> getList() {
        return items;
    }

    public void addItemInLast(T item) {
        items.add(0, item);
        notifyDataSetChanged();
        //notifyItemInserted(0);
    }

    public void addAll(List<T> list) {
        items.addAll(list);
        //notifyDataSetChanged();
    }

    public void clearList() {
        items.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private Map<Integer, View> views;
        private View view;

        public ViewHolder(View view, OnViewHolderClick listener) {
            super(view);
            views = new HashMap<>();
            views.put(0, view);

            this.view = view;

            if (listener != null) {
                view.setOnLongClickListener(this);
                view.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            try {
                listener.onRecyclerItemClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public boolean onLongClick(View view) {
            try {
                listener.onRecyclerItemLongClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }

            return false;
        }

        public void initViewList(int[] idList) {
            for (int id : idList)
                initViewById(id);
        }

        public void initViewById(int id) {
            View view = (getView() != null ? getView().findViewById(id) : null);

            if (view != null)
                views.put(id, view);
        }

        public View getView() {
            return view;
        }


    }
}


