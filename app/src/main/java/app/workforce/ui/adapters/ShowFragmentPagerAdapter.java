package app.workforce.ui.adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import app.workforce.fragments.abstracts.BaseFragment;

import java.util.ArrayList;

public class ShowFragmentPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<BaseFragment> mFragmentCollection;
    private FragmentManager fm;


    public ShowFragmentPagerAdapter(FragmentManager fm, ArrayList<BaseFragment> fragmentCollection) {
        super(fm);
        this.fm = fm;
        mFragmentCollection =  fragmentCollection;
    }

    @Override
    public Fragment getItem(int position) {
       return mFragmentCollection.get(position);
   }

    @Override
    public int getCount() {
        return mFragmentCollection.size();
    }
}
