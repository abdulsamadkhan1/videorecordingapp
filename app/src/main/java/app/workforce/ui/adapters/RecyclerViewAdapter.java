package app.workforce.ui.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class RecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    protected List<T> items;
    private Context context;
    private RecyclerViewAdapter.OnViewHolderClick<T> listener;

    public RecyclerViewAdapter(Context context, RecyclerViewAdapter.OnViewHolderClick<T> listener) {

        this.context = context;
        this.listener = listener;
        items = new ArrayList<>();

    }

    public interface OnViewHolderClick<T> {
        void onRecyclerItemLongClick(View view, int position, T item);

        void onRecyclerItemClick(View view, int position, T item);
    }

    protected abstract View createView(Context context, ViewGroup viewGroup, int viewType);

    protected abstract void bindView(T item, RecyclerViewAdapter.ViewHolder viewHolder, int position);

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new RecyclerViewAdapter.ViewHolder(createView(context, viewGroup, viewType), listener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        bindView(getItem(position), holder, position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public T getItem(int index) {
        return ((items != null && index < items.size()) ? items.get(index) : null);
    }

    public Context getContext() {
        return context;
    }

    public void setList(List<T> list) {
        items = list;
    }

    public List<T> getList() {
        return items;
    }

    public void addItemInLast(T item) {
        items.add(0, item);
        notifyItemInserted(0);
    }

    public void addAll(List<T> list) {
        items.addAll(list);
        //notifyDataSetChanged();
    }

    public void clearList() {
        items.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private Map<Integer, View> views;
        private View view;

        public ViewHolder(View view, RecyclerViewAdapter.OnViewHolderClick listener) {
            super(view);
            views = new HashMap<>();
            views.put(0, view);

            this.view = view;

            if (listener != null) {
                view.setOnLongClickListener(this);
                view.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            try {
                listener.onRecyclerItemClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public boolean onLongClick(View view) {
            try {
                listener.onRecyclerItemLongClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }

            return false;
        }

        public void initViewList(int[] idList) {
            for (int id : idList)
                initViewById(id);
        }

        public void initViewById(int id) {
            View view = (getView() != null ? getView().findViewById(id) : null);

            if (view != null)
                views.put(id, view);
        }

        public View getView() {
            return view;
        }


    }
}
