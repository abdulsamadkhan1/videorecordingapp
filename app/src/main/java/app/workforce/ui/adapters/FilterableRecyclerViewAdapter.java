package app.workforce.ui.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.google.common.base.Function;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class FilterableRecyclerViewAdapter<T> extends RecyclerView.Adapter<FilterableRecyclerViewAdapter.ViewHolder> implements Filterable {

    private List<T> originalList;
    private List<T> filteredList;
    ToStringFilter toStringFilter = new ToStringFilter();

    private Context context;
    private FilterableRecyclerViewAdapter.OnViewHolderClick<T> listener;

    public FilterableRecyclerViewAdapter(Context context, List<T> arrayList, FilterableRecyclerViewAdapter.OnViewHolderClick<T> listener, Function<T, String> converter) {

        this.context = context;
        this.listener = listener;

        originalList = new ArrayList<T>(arrayList);
        filteredList = new ArrayList<T>(arrayList);

        if (converter != null) {
            toStringFilter = new ToStringFilter(converter);
        }

    }

    @Override
    public Filter getFilter() {
        return toStringFilter;
    }

    public class ToStringFilter extends Filter {

        private CharSequence lastConstrant;
        Function<T, String> converter;

        public ToStringFilter(Function<T, String> converter) {
            this.converter = converter;
        }

        public ToStringFilter() {
        }

        protected void notifyFilter() {
            filter(lastConstrant);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            this.lastConstrant = constraint;
            FilterResults results = new FilterResults();

            if (Strings.isNullOrEmpty(constraint.toString())) {
                results.count = originalList.size();
                results.values = new ArrayList<T>(originalList);
                return results;
            }

            ArrayList<T> filterList = new ArrayList<T>();
            constraint = constraint.toString().toLowerCase();

            for (int i = 0; i < originalList.size(); i++) {
                if (converter != null) {
                    String apply = converter.apply(originalList.get(i));
                    try {
                        if (apply.toLowerCase().contains(constraint)) {
                            filterList.add(originalList.get(i));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (originalList.get(i).toString().toLowerCase()
                        .contains(constraint.toString())) {
                    filterList.add(originalList.get(i));
                }

            }

            results.count = filterList.size();
            results.values = filterList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredList = (List<T>) results.values;
            notifyDataSetChanged();
        }
    }

    public interface OnViewHolderClick<T> {
        void onRecyclerItemLongClick(View view, int position, T item);

        void onRecyclerItemClick(View view, int position, T item);
    }

    protected abstract View createView(Context context, ViewGroup viewGroup, int viewType);

    protected abstract void bindView(T item, FilterableRecyclerViewAdapter.ViewHolder viewHolder, int position);


    @Override
    public FilterableRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new FilterableRecyclerViewAdapter.ViewHolder(createView(context, viewGroup, viewType), listener);
    }

    @Override
    public void onBindViewHolder(FilterableRecyclerViewAdapter.ViewHolder holder, int position) {
        bindView(getItem(position), holder, position);
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public T getItem(int index) {
        return ((filteredList != null && index < filteredList.size()) ? filteredList.get(index) : null);
    }

    public Context getContext() {
        return context;
    }

    public void setList(List<T> list) {
        filteredList = list;
    }

    public List<T> getList() {
        return filteredList;
    }

    public void addItemInLast(T item) {
        filteredList.add(0, item);
        notifyItemInserted(0);
    }

    public void addAll(List<T> list) {
        filteredList.addAll(list);
        //notifyDataSetChanged();
    }

    public void clearList() {
        filteredList.clear();
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private Map<Integer, View> views;
        private View view;

        public ViewHolder(View view, FilterableRecyclerViewAdapter.OnViewHolderClick listener) {
            super(view);
            views = new HashMap<>();
            views.put(0, view);

            this.view = view;

            if (listener != null) {
                view.setOnLongClickListener(this);
                view.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View view) {
            try {
                listener.onRecyclerItemClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public boolean onLongClick(View view) {
            try {
                listener.onRecyclerItemLongClick(view, getAdapterPosition(), getItem(getAdapterPosition()));
            }catch (Exception e){
                e.printStackTrace();
            }

            return false;
        }

        public void initViewList(int[] idList) {
            for (int id : idList)
                initViewById(id);
        }

        public void initViewById(int id) {
            View view = (getView() != null ? getView().findViewById(id) : null);

            if (view != null)
                views.put(id, view);
        }

        public View getView() {
            return view;
        }


    }

}
