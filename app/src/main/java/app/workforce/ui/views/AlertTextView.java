package app.workforce.ui.views;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import app.workforce.R;
import app.workforce.autolinktextview.AutoLinkMode;
import app.workforce.autolinktextview.AutoLinkOnClickListener;
import app.workforce.autolinktextview.AutoLinkTextView;
import app.workforce.autolinktextview.RegexParser;
import app.workforce.interfaces.ITextViewSpanClick;

public class AlertTextView extends AutoLinkTextView {

    private Context context;

    private ITextViewSpanClick textViewSpanClick;

    public AlertTextView(Context context) {
        super(context);

        this.context = context;

        setLinksClickable();
    }

    public AlertTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        if (!isInEditMode()) {
            Util.setTypeface(attrs, this);
        }

        setLinksClickable();
    }

    public void setSpanClickDelegate(ITextViewSpanClick textViewSpanClick) {
        this.textViewSpanClick = textViewSpanClick;
    }


    private void setLinksClickable() {

        addAutoLinkMode(
                AutoLinkMode.MODE_PHONE,
                AutoLinkMode.MODE_URL,
                AutoLinkMode.MODE_EMAIL,
                AutoLinkMode.MODE_CUSTOM);


        setPhoneModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setUrlModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setEmailModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setCustomModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setCustomRegex(RegexParser.CUSTOM_REGEX_PATTERN);

        setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {
                textViewSpanClick.onSpanClick(matchedText);
            }
        });

    }

}
