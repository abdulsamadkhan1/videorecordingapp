//package app.workforce.ui.views;
//
//import android.content.Context;
//import androidx.recyclerview.widget.RecyclerView;
//import android.util.AttributeSet;
//import android.view.MotionEvent;
//
//import app.workforce.fragments.CustomSupportMapFragment;
//
//public class CustomRecyclerView extends RecyclerView {
//
//    private CustomSupportMapFragment.OnTouchListener mListener;
//
//    public CustomRecyclerView(Context context) {
//        super(context);
//    }
//
//    public CustomRecyclerView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public void setListener(CustomSupportMapFragment.OnTouchListener listener) {
//        mListener = listener;
//    }
//
//    public interface OnTouchListener {
//        void onTouch();
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                mListener.onTouch();
//                break;
//            case MotionEvent.ACTION_UP:
//                mListener.onTouch();
//                break;
//        }
//        return super.dispatchTouchEvent(event);
//    }
//}
