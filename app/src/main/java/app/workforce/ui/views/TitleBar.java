package app.workforce.ui.views;

import android.content.Context;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import app.workforce.R;


public class TitleBar extends Toolbar {

    private AnyTextView txtTitle;
    private ImageView btnLeft;
    private ImageView btnRight;
    private ImageView btnRight2;
    private ImageView btnRight3;
    private ImageView btnRight4;
    private AnyTextView txtRight;
    private View headerView;

    private TextView txtBadge;

    private View.OnClickListener menuButtonListener;
    private OnClickListener backButtonListener;

    private Context context;


    public TitleBar(Context context) {
        super(context);
        this.context = context;
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {
    }

    private void bindViews() {

        txtTitle = (AnyTextView) this.findViewById(R.id.txtTitle);
        btnRight = (ImageView) this.findViewById(R.id.btnRight);
        btnRight2 = (ImageView) this.findViewById(R.id.btnRight2);
        btnRight3 = (ImageView) this.findViewById(R.id.btnRight3);
        btnRight4 = (ImageView) this.findViewById(R.id.btnRight4);
        btnLeft = (ImageView) this.findViewById(R.id.btnLeft);
        txtRight = (AnyTextView) this.findViewById(R.id.txtRight);

        txtBadge = (TextView) this.findViewById(R.id.txtBadge);

    }

    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        headerView = inflater.inflate(R.layout.header_main, this);
        bindViews();
    }

    public void hideButtons() {
        txtBadge.setVisibility(View.GONE);

        txtTitle.setVisibility(View.GONE);
        btnLeft.setVisibility(View.GONE);
        btnRight.setVisibility(View.GONE);
        btnRight2.setVisibility(View.GONE);
        btnRight3.setVisibility(View.GONE);
        btnRight4.setVisibility(View.GONE);
        txtRight.setVisibility(View.GONE);


    }

    public void showBackButton() {
        btnLeft.setImageResource(R.drawable.img_back);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(backButtonListener);

    }

    public void showBackButton(OnClickListener clickListener) {
        btnLeft.setImageResource(R.drawable.img_back);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(clickListener);

    }

    public void showMultiMediaIcon(int resIcon, OnClickListener clickListener) {
        btnRight3.setVisibility(View.VISIBLE);
        btnRight3.setOnClickListener(clickListener);
        btnRight3.setImageResource(resIcon);
    }

    public void showMenuButton() {
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(menuButtonListener);
        btnLeft.setImageResource(R.drawable.img_menu);
    }

    public void setSubHeading(String heading) {
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(heading);
    }

    public TextView getTitleText() {
        return txtTitle;
    }

    public void showNotificationBadge(Context context, String notificationCount) {

        txtBadge.setVisibility(View.VISIBLE);
        txtBadge.setText(notificationCount);

    }

    public void resetNotificationCount() {
        txtBadge.setVisibility(View.GONE);
    }

    public void showRightHeading(String text, OnClickListener clickListener) {
        txtRight.setVisibility(View.VISIBLE);
        txtRight.setText(text);
        txtRight.setOnClickListener(clickListener);
    }

    public void showTitleBar() {
        this.setVisibility(View.VISIBLE);
    }

    public void hideTitleBar() {
        this.setVisibility(View.GONE);
    }

    public void setMenuButtonListener(View.OnClickListener listener) {
        menuButtonListener = listener;
    }

    public void setBackButtonListener(View.OnClickListener listener) {
        backButtonListener = listener;
    }


}
