package app.workforce.ui.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import app.workforce.R;
import app.workforce.autolinktextview.AutoLinkMode;
import app.workforce.autolinktextview.AutoLinkOnClickListener;
import app.workforce.autolinktextview.AutoLinkTextView;
import app.workforce.utils.Utils;

public class AnyTextView extends AutoLinkTextView {

    private Context context;

    public AnyTextView(Context context) {
        super(context);

        this.context = context;

        setLinksClickable();
    }

    public AnyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        if (!isInEditMode()) {
            Util.setTypeface(attrs, this);
        }

        setLinksClickable();
    }


    private void setLinksClickable() {

        addAutoLinkMode(
                AutoLinkMode.MODE_HASHTAG,
                AutoLinkMode.MODE_PHONE,
                AutoLinkMode.MODE_URL,
                AutoLinkMode.MODE_EMAIL,
                AutoLinkMode.MODE_MENTION);

        setHashtagModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setPhoneModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setCustomModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setMentionModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setUrlModeColor(ContextCompat.getColor(context, R.color.theme_blue));
        setEmailModeColor(ContextCompat.getColor(context, R.color.theme_blue));

        setAutoLinkOnClickListener(new AutoLinkOnClickListener() {
            @Override
            public void onAutoLinkTextClick(AutoLinkMode autoLinkMode, String matchedText) {

                matchedText = matchedText.trim();

                if (Utils.validateURL(matchedText)) {
                    if (!matchedText.startsWith("http://") && !matchedText.startsWith("https://")) {
                        matchedText = "http://" + matchedText;
                    }
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(matchedText));
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (Utils.validateEmail(matchedText)) {
                    try {
                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                "mailto", matchedText, null));
                        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (Utils.validatePhone(matchedText)) {
                    try {
                        Intent i = new Intent(Intent.ACTION_DIAL,
                                Uri.parse("tel:" + matchedText));
                        context.startActivity(i);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }
        });

    }

}
