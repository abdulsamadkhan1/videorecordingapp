package app.workforce.interfaces;

import android.view.View;

public interface IDrawableClickListener {

    public static enum DrawablePosition {
        TOP,
        BOTTOM,
        LEFT,
        RIGHT
    }



    public void onClick(View view, DrawablePosition target);
}
