package app.workforce.interfaces;

public interface IOnLoadMoreListener {

    void onLoadMore(int currentPage);
}
