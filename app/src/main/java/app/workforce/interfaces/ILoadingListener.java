package app.workforce.interfaces;

public interface ILoadingListener {

    void onLoadingStarted();

    void onLoadingFinished();

    /**
     * @param percentLoaded should be between 1 and 100
     */
    void onProgressUpdated(int percentLoaded);
}
