package app.workforce.interfaces;

public interface ITextViewSpanClick {

    void onSpanClick(String matchedText);

}
