package app.workforce.interfaces;

public interface IPermissionResult {

    void onPermissionGranted(int viewId);
}
