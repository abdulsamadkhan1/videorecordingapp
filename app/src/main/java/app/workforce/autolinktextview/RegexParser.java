package app.workforce.autolinktextview;

import android.util.Patterns;



public class RegexParser {

    public static final String PHONE_PATTERN = Patterns.PHONE.pattern();
    public static final String EMAIL_PATTERN = Patterns.EMAIL_ADDRESS.pattern();
    public static final String HASHTAG_PATTERN = "(?:^|\\s|$)#[\\p{L}0-9_]*";
    public static final String MENTION_PATTERN = "(?:^|\\s|$|[.])@[\\p{L}0-9_]*";
    public static final String URL_PATTERN = "(^|[\\s.:;?\\-\\]<\\(])" +
            "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" +
            "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])";
    public static final String CUSTOM_REGEX_PATTERN = ".Post|.Blog|.Terms & Conditions|.Terms|.post|.blog|.terms & conditions";
}
