package app.workforce.autolinktextview;



public interface AutoLinkOnClickListener {

    void onAutoLinkTextClick(AutoLinkMode autoLinkMode,String matchedText);
}
