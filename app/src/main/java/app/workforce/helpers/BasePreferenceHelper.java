package app.workforce.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class BasePreferenceHelper extends PreferenceHelper {

    private Context context;

    private static final String FILENAME = "preferences";

    //For General Sessions
    protected static final String KEY_LOGIN_STATUS = "islogin";

    protected static final String KEY_CHECKING_URL = "ISURL";




    //For Handling Life Cycle
    protected static final String KEY_ISS_APP_BG = "appInBG";

    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    //----------------- For General Sessions -------------------------------------
    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }


    public void setUrlKey(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_CHECKING_URL, isLogin);
    }

    public boolean getUrlKey() {
        return getBooleanPreference(context, FILENAME, KEY_CHECKING_URL);
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

//----------------- For Handing Life Cycle -------------------------------------

    public void setIsAppBG(boolean isAppBG) {
        putBooleanPreference(context, FILENAME, KEY_ISS_APP_BG, isAppBG);
    }

    public boolean isAppBG() {
        return getTrueBooleanPreference(context, FILENAME, KEY_ISS_APP_BG);
    }

}
