package app.workforce.helpers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import app.workforce.activities.MainActivity;
import app.workforce.fragments.abstracts.BaseFragment;

public class CameraHelper {

    private static Intent pictureActionIntent;

    public static void uploadPhotoDialog(final MainActivity activity, final BaseFragment fragment) {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(activity);
        myAlertDialog.setTitle("Upload Photo");
        myAlertDialog.setMessage("How do you want to set your photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
//                        activity.chooseImage(fragment);
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
//                        activity.takePicture(fragment);
                    }
                });
        myAlertDialog.show();
    }

   /* private static String image_path_string = "";

    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;

    private static Uri uriImage;
    private static File imageFile;

    public static void uploadFromGallery(Fragment fragment) {

        pictureActionIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        pictureActionIntent.setType("image/*");
        pictureActionIntent.putExtra("return-data", false);
        fragment.startActivityForResult(pictureActionIntent, AppConstants.GALLERY_REQUEST);

    }
*/

    /*public static void uploadFromCamera(Fragment fragment) {

        imageFile = FileHelper.getScammerProfileImageFile();
        uriImage = Uri.fromFile(imageFile);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImage);
        fragment.startActivityForResult(intent, AppConstants.CAMERA_REQUEST);
    }

    public static Uri getImageUri() {
        return uriImage;
    }

    public static File getImageFile(){
        return imageFile;
    }


    public static Bitmap retrieveAndDisplayPictureOfDrawableSize(
            int requestCode, int resultCode, Intent data, Activity activity,
            ImageView imgPhoto,  Uri imageUri) {

        switch (requestCode) {

            case AppConstants.GALLERY_REQUEST:
                try {
                    Uri selectedImageUri = data.getData();
                    image_path_string = Funcs.getPath(selectedImageUri,
                            activity);

                    Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                            getOptions());
                    if (bmp != null) {
                        bmp = BitmapHelper.scaleCenterCrop(bmp, HEIGHT, WIDTH);
                    }
                    bmp = BitmapHelper.getImageOrientation(image_path_string,
                            bmp);
                    image_path_string = Funcs.replace(activity, bmp,
                            image_path_string);
                    ImageLoader.getInstance().displayImage(
                            "file:///" + image_path_string, imgPhoto);

                    return bmp;

                } catch (Exception e) {
                    if (activity != null)
                        Funcs.showShortToast(
                                "Unable to get image. Please try again..",
                                activity);
                    return null;

                } catch (Throwable e) {
                    e.printStackTrace();
                    if (activity != null)
                        Funcs.showShortToast(
                                "Unable to get image. Please try again..",
                                activity);
                    return null;
                }

            case AppConstants.CAMERA_REQUEST:
                try {
                    Uri selectedImageUri = imageUri;
                    image_path_string = Funcs.getPath(selectedImageUri,
                            activity);

                    Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                            getOptions());
                    if (bmp != null) {
                        bmp = BitmapHelper.scaleCenterCrop(bmp, HEIGHT, WIDTH);
                    }
                    bmp = BitmapHelper.getImageOrientation(image_path_string,
                            bmp);
                    image_path_string = Funcs.replace(activity, bmp,
                            image_path_string);
                    ImageLoader.getInstance().displayImage(
                            "file:///" + image_path_string, imgPhoto);

                    return bmp;

                } catch (Exception e) {
                    if (activity != null)
                        Funcs.showShortToast(
                                "Unable to get image. Please try again..",
                                activity);
                    return null;

                } catch (Throwable e) {
                    e.printStackTrace();
                    if (activity != null)
                        Funcs.showShortToast(
                                "Unable to get image. Please try again..",
                                activity);
                    return null;
                }

            default:
                return null;
        }

    }*/


/*    private static BitmapFactory.Options getOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return options;
    }*/


  /*  public static void uploadPicture(Activity activity, final Fragment fragment) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Choose BlogImage Source");
        builder.setCancelable(true);
        builder.setItems(new CharSequence[]{"Gallery", "Camera"},
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                // GET IMAGE FROM THE GALLERY
                                uploadFromGallery(fragment);
                                break;

                            case 1:
                                // GET IMAGE FROM THE CAMERA
                                uploadFromCamera(fragment);
                                break;

                        }
                    }
                });

        builder.show();

    }*/

}
