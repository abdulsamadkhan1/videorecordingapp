package app.workforce.observer;

import java.util.Observable;

public class ObservableRegistrar<T> extends Observable {

    private static ObservableRegistrar INSTANCE = null;

    public void setChangedObject(T object){
        setChanged();
        notifyObservers(object);
    }


    public static ObservableRegistrar getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new ObservableRegistrar();
        }
        return INSTANCE;
    }



}
